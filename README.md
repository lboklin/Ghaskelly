# Ghaskelly

*Note: This library is not in any remotely finished or working state. It's not
tested in practice at all and at this point is still only an experiment.*

Ghaskelly is attempt at implementing a purely functional API for developing
games with Godot in Haskell. The goal is for the API design to closely resemble
that of The Elm Architecture, where you use messages to manage effects in a safe
manner.

Ghaskelly should function like a runtime, handling all IO and stateful
side-effects behind the scene, passing the results back in the form of a message
data type defined by the developer, containing the result of the operation.

The motivation for this is to allow you as a developer to focus on describing
your game in terms of data flow, data structures and pure mathematical
expressions, and spare you from having to worry about the state inconsistencies
and black magic invisibly messing with your values that comes with imperative
programming.


## Usage:

Don't (yet). 

However, the library is going to be tested and demoed using [template project](https://gitlab.com/lboklin/Ghaskelly-template) 
as a base which will, once usable, be the recommended way of quickly getting
started.
