{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ExistentialQuantification #-}
module Ghaskelly
  ( module Exports
  , module Errors
  , Game(..)
  , initGhaskelly
  )
where


-- EXPORTS --

import           Godot.Gdnative.Internal       as Errors
                                                          ( GodotError(..)
                                                          , GodotVariantCallError
                                                          )

import           Ghaskelly.Prelude             as Exports

import           Godot.Gdnative.Types          as Exports
                                                          ( toLowLevel
                                                          , fromLowLevel
                                                          )

import           Godot.Extra                   as Exports

import           Ghaskelly.Scene               as Exports
import           Ghaskelly.Sub                 as Exports
import           Ghaskelly.Cmd                 as Exports

-------------


import           Data.Vector                              ( (!?) )

import           Godot.Gdnative.Types                     ( LibType(..)
                                                          , TypeOf
                                                          , GodotFFI
                                                          , AsVariant
                                                          )
import           Godot.Gdnative.Internal.Gdnative
import           Godot.Internal.Dispatch                  ( HasBaseClass(..)
                                                          , BaseClass
                                                          )

import           Godot.Extra.Register

import           Ghaskelly.Cmd.Internal
import           Ghaskelly.Sub.Internal


-- | The initialization to call from the FFI exports
initGhaskelly :: GdnativeHandle -> Game model msg -> IO ()
initGhaskelly desc newGame = do
  registerClass $ RegClass desc $ \o -> Ghaskelly o <$> newTVarIO newGame


-- | Game entry point
data Game model msg = Game
  { initial  :: GodotNode -> (model, Cmd msg)
  -- ^ initial model with possible effects
  , update :: msg -> model -> (model, Cmd msg)
  -- ^ Function to update model, optionally provide effects.
  , subscriptions   :: model -> Sub msg
  -- ^ List of subscriptions to upd during application lifetime
  {-, scene :: model -> Scene msg-}
  }


-- | The Ghaskelly class which we use to carry our state and so on
data Ghaskelly = forall model msg. Ghaskelly
  { self :: GodotObject
  , game   :: TVar (Game model msg)
  }

instance HasBaseClass Ghaskelly where
  type BaseClass Ghaskelly = GodotNode
  super Ghaskelly{ self } = GodotNode self

instance GodotClass Ghaskelly where
  godotClassName = "Ghaskelly"

instance ClassExport Ghaskelly where
  classExtends  = "Node"

  -- Will be ignored and reinitialized later because we don't know the types yet
  classInit obj = Ghaskelly obj <$> newTVarIO (error "Game not yet initialized")

  classMethods  =
    [ GodotMethod NoRPC "_ready" _ready
    , GodotMethod NoRPC "_input" _input
    , GodotMethod NoRPC "_physics_process" _physicsProcess
    , GodotMethod NoRPC "_process" _process
    ]


-- Class methods

-- | This is run by Godot as soon as our class has been fully instantiated.
-- Here we update the initial model from the initial commands.
_ready :: Ghaskelly -> Vector GodotVariant -> IO GodotVariant
_ready Ghaskelly { self, game } _ = do
  godotPrint "Ghaskelly is ready."

  model <- readTVarIO game
    >>= \Game { update, initial } -> runCmds update (initial (GodotNode self))

  atomically $ modifyTVar' game $ \gm -> gm { initial = (\slf -> (model, [])) }

  retnil


-- | Whenever input happens, this will be called.
_input :: Ghaskelly -> Vector GodotVariant -> IO GodotVariant
_input Ghaskelly { self, game } args = do
  iev <- (args & getHighArg' 0 :: IO (HighLevelOf GodotObject))
    <&> withHigh GodotInputEvent

  atomically <=< updateGame (GodotNode self) game $ \case
    Input sub -> Just $ sub (GodotNode self) iev
    _         -> Nothing

  unref iev
  retnil


-- | Called every physics frame. Default is 60 hz but is configurable in Godot.
-- Here's where physics calculations should take place.
_physicsProcess :: Ghaskelly -> Vector GodotVariant -> IO GodotVariant
_physicsProcess Ghaskelly { self, game } args = do
  High delta <- args & getHighArg' 0 :: IO (HighLevelOf Float)

  atomically <=< updateGame (GodotNode self) game $ \case
    PhysicsProcess sub -> Just $ sub self delta
    _                  -> Nothing

  retnil


-- | Called every frame.
_process :: Ghaskelly -> Vector GodotVariant -> IO GodotVariant
_process Ghaskelly { self, game } args = do
  High delta <- args & getHighArg' 0 :: IO (HighLevelOf Float)

  atomically <=< updateGame (GodotNode self) game $ \case
    Process sub -> Just $ sub self delta
    _           -> Nothing

  retnil


--- Stuff


-- | Run the subs from the callback picked by @fromCb@, then run all the accumulated
-- commands and, finally, return the atomic transaction to perform on 'Game model msg'.
updateGame
  :: GodotNode
  -> TVar (Game model msg)
  -> (Callback msg -> Maybe msg)
  -> IO (STM ())
updateGame node game fromCb = do
  (update, initial, subs) <- atomically $ do
    Game { initial, update, subscriptions } <- readTVar game
    let subs = subscriptions $ fst $ initial node
    return (update, initial, subs)

  model <- initial node & runSubs update (SubMap fromCb subs) & runCmds update

  return $ modifyTVar' game $ \gm@Game { initial } ->
    gm { initial = first (const model) . initial }
