module Ghaskelly.Prelude
  ( module U
  , module G
  , print
  )
where

import           Universum                     as U
                                                   hiding ( get
                                                          , set
                                                          , init
                                                          , show
                                                          , print
                                                          )
import           Ghaskelly.Godot               as G
                                                   hiding ( sub
                                                          , _ready
                                                          , _process
                                                          , _input
                                                          , update
                                                          , print
                                                          )


print :: Text -> IO ()
print = godotPrint
