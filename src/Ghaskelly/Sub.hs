-- | This module is meant to be imported qualified by Ghaskelly projects.
module Ghaskelly.Sub
  ( Sub
  , noSub
  , sub
  , subs
  , onInput
  , onPhysicsProcess
  , onProcess
  )
where

import           Ghaskelly.Prelude

import           Ghaskelly.Sub.Internal


noSub :: model -> Sub msg
noSub _ = []

sub :: (model -> Callback msg) -> model -> Sub msg
sub toSub model = toSub model : []

subs :: [Callback msg] -> Sub msg
subs = id

---

onInput :: (GodotNode -> GodotInputEvent -> msg) -> Callback msg
onInput = Input

onPhysicsProcess :: (GodotObject -> Float -> msg) -> Callback msg
onPhysicsProcess = PhysicsProcess

onProcess :: (GodotObject -> Float -> msg) -> Callback msg
onProcess = Process
