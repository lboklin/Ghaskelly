{-# LANGUAGE ScopedTypeVariables #-}
module Ghaskelly.Cmd.Internal where

import           Ghaskelly.Prelude

import           Control.Concurrent.Async
                                                          ( Async
                                                          , async
                                                          , waitAny
                                                          )


-- | A 'Cmd msg' is a list of messages produced from IO actions.
type Cmd msg = [ IO msg ]


-- | Updates the current model by running the commands.
runCmds :: forall model msg.
  (msg -> model -> (model, Cmd msg))
  -> (model, Cmd msg)
  -> IO model
runCmds update (model, cmds) = do
  initAsyncs <- traverse async cmds
  run' update initAsyncs model
 where
  run' :: forall model msg.
    (msg -> model -> (model, Cmd msg))
    -> [Async msg]
    -> model
    -> IO model
  run' update asyncs model =
    -- ^ Asynchronously execute the IO actions from command list and update the
    -- model with the messages
    if null asyncs
    then return model
    else do
      -- This works like a pool of async commands with a queue
      -- in the end. The first command to be resolved is the first
      -- command dealt with.
      (completedCmd, msg) <- waitAny asyncs :: IO (Async msg, msg)

      let (newModel, newCmds) = update msg model

      newCmdsAsync <- traverse async newCmds

      let newAsyncs =
            filter (/= completedCmd) -- Remove the cmd that we just ran
            asyncs ++ newCmdsAsync   -- Let's add what our update returned

      run' update newAsyncs newModel
