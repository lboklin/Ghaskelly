-- | This module is meant to be imported qualified by Ghaskelly projects.
module Ghaskelly.Cmd
  ( Cmd
  , noCmd
  , cmd
  , cmds
  )
where

import           Ghaskelly.Prelude

import           Ghaskelly.Cmd.Internal                   ( Cmd )


noCmd :: model -> (model, Cmd msg)
noCmd model = (model, [])

cmd :: IO msg -> model -> (model, Cmd msg)
cmd cmd model = (model, [cmd])

cmds :: Cmd msg -> model -> (model, Cmd msg)
cmds cmds model = (model, cmds)
