-- | This is just in case you feel like making your code look more Elmish.
module Ghaskelly.AltOperators where


import           Ghaskelly.Prelude                 hiding ( (>>) )

import           Control.Category                         ( (>>>) )


(|>) :: a -> (a -> b) -> b
(|>) = (&)
infixl 1 |>

(>>) :: (a -> b) -> (b -> c) -> (a -> c)
(>>) = (>>>)
infixr 1 >>

(<|) :: (a -> b) -> a -> b
(<|) = ($)
infixr 0 <|

(<<) :: (b -> c) -> (a -> b) -> a -> c
(<<) = (.)
infixr 9 <<
