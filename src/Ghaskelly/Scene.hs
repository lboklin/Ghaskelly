{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
module Ghaskelly.Scene where

import           Ghaskelly.Prelude

import           Godot.Gdnative.Internal                  ( godot_variant_get_type
                                                          )
import           Godot.Gdnative.Types
import           Godot.Internal.Dispatch                  ( (:<) )


type Scene msg = GodotNode -> NodeTree

data SceneTree = SceneTree GodotSceneTree NodeTree


-- | A type to represent a node with children nodes, with its first
-- field being a constructor for the type we really want to instance.
data NodeTree = forall a. NodeTree (GodotObject -> a) GodotNode [NodeTree]

-- One day I'll figure out a way to handle properties..
type Properties = [(Text, Variant 'GodotTy)]

-- | An alias for a NodeTree waiting to be fully constructed.
-- This is mainly because a node entry point is necessary
-- in order to instance new nodes. Example usage:
-- @
-- scene = \_ ->
--   children
--     [ GodotSpatial <: []
--     , GodotCamera <: []
--     ]
-- @
type NTree = GodotNode -> NodeTree


-- | An operator for constructing NTrees in a readable fashion.
(<:) :: GodotNode :< a => (GodotObject -> a) -> [NTree] -> NTree
pConstr <: cs = \nd -> NodeTree pConstr nd (fmap ($ nd) cs)


-- | Construct children NTrees - assumes the parent is a GodotNode.
children :: [NTree] -> NTree
children = (<:) GodotNode


getSceneTree :: GodotNode -> IO SceneTree
getSceneTree nd = do
  tree <- nd & get_tree
  n    <- tree & get_root >>= (`get_child` 0)
  -- ^ Get the top node in our nodeTree tree
  SceneTree tree <$> getNodeTree n


getNodeTree :: GodotNode -> IO NodeTree
getNodeTree nd = getChildren nd >>= mapM getNodeTree <&> NodeTree GodotNode nd


getChildren :: GodotNode -> IO [GodotNode]
getChildren nd = do
  children <- get_children nd
  mapM fromGodotVariant =<< fromLowLevel children


-- Ignore following scribbles


{-
 -checkPropertiesEq :: Properties -> Properties -> IO Bool
 -checkPropertiesEq ((pNameA, pVarA):propsA) ((pNameB, pVarB):propsB) = do
 -  lowA <- toLowLevel pVarA
 -  lowB <- toLowLevel pVarB
 -  tyA <- godot_variant_get_type lowA
 -  tyB <- godot_variant_get_type lowB
 -  hiA <- fromGodotVariant lowA
 -  hiB <- fromGodotVariant lowB
 -  if pNameA == pNameB && tyA == tyB
 -    then return $
 -      case (hiA, hiB) of
 -        (Just a, Just b) -> (a == b)
 -        _ -> False
 -    else return False
 -}


{-
 -checkNodeEq :: NTree -> NTree -> IO Bool
 -checkNodeEq (NTree ndA propsA) (NTree ndB propsB) = do
 -
 -
 -
 -checkTreeEq :: NodeTree -> NodeTree -> IO Bool
 -checkTreeEq (NodeTree nodeA (childA:childrenA)) (NodeTree nodeB (childB:childrenB)) = do
 -  nodesEq <- checkNodeEq nodeA nodeB
 -  if nodesEq
 -    then checkTreeEq (NodeTree childA childrenA) (NodeTree childB childrenB)
 -    else return False
 -}
