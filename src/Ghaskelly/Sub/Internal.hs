{-# LANGUAGE RankNTypes #-}
module Ghaskelly.Sub.Internal where

import           Control.Concurrent.Async
                                                          ( Async
                                                          , async
                                                          , waitAny
                                                          )

import           Godot.Extra
import           Ghaskelly.Prelude
import           Ghaskelly.Cmd


type Sub msg = [ Callback msg ]


-- | Holds a function that takes the parameters of their respective
-- callback and creates a message when run inside them.
data Callback msg
  = Input          (GodotNode   -> GodotInputEvent -> msg)
  | PhysicsProcess (GodotObject -> Float           -> msg)
  | Process        (GodotObject -> Float           -> msg)



-- | A 'SubRunner' takes a function to run inside a callback and produces any
-- message produced.
data SubMap msg = SubMap (Callback msg -> Maybe msg) (Sub msg)


-- | Uses the 'SubRunner msg' to select the subscriptions to run and then updates
-- the model with any commands returned.
runSubs :: forall model msg.
  (msg -> model -> (model, Cmd msg))
  -> SubMap msg
  -> (model, Cmd msg)
  -> (model, Cmd msg)
runSubs update (SubMap fromCb subs) current =
  fromCb `map` subs
  -- ^ Process the subs into messages.
    & catMaybes
    -- ^ Strip out the ones not selected by the SubMap.
    & foldr' sumUpdates current
    -- ^ Update the model for each message produced and add
    -- the new commands to the old ones.
 where
  sumUpdates msg (model, cmds) =
    update msg model
      & second (cmds ++)
